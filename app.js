const fetch = require('node-fetch');
const { URLSearchParams } = require('url');
const fs = require('fs') 
const listFingerprint = "./config/FingerprintList.dat"
const listQueue = "./config/Queue.json"

const id = "sttkd"

let dummy = [
    {
        PIN: '3',
        VerifyMode: 1,
        ScanDate: '2019-04-27 03:02:17'
    }
]

async function getQueue() {
    let rawdata = fs.readFileSync(listQueue);
    try{
        return JSON.parse(rawdata)
    }catch(err){
        return []
    }
}

async function sendToServer(){
    queueData =  await getQueue()
    for(let index in queueData){
    fetch(`https://tool.stechoq.com/hooks/fingerprint/${id}`, {
        method: 'post',
        headers:{
            "Content-Type":"application/json"
        },
        body: JSON.stringify(queueData[index])
    })
        .then(res => res.json())
        .then(response => {
            console.log(queueData[index].ScanDate+" - sended");
            fs.writeFileSync(listQueue, "[]");
        }).catch(err => {
            console.log("current data entering queue");
        })
    }
}

async function queueing(currentData) {
    let queueList =  await getQueue()
    let storeQueue = queueList.concat(currentData)
    fs.writeFileSync(listQueue, JSON.stringify(storeQueue));
    await sendToServer()
}

function scanNewLog(fileDir) {
    var lineReader = require('readline').createInterface({
        input: require('fs').createReadStream(fileDir)
    });

    lineReader.on('line', function (line) {
        fetch('http://localhost:8080/scanlog/new', {
            method: 'post',
            body: `sn=${line}`
        })
            .then(res => {
                console.log(JSON.stringify(res));
                
            })
            .then(response => {
                console.log(line);
                
                if (response['Result'] == true) {
                    console.log(response['Data']);
                    queueing(response['Data'])
                    // queueing(dummy)
                } else {
                    console.log("Error : " + response.message);
                }
            }).catch(err => {
                console.log(err);
            })
    })
}


function timeoutFunc() {
    var today = new Date();
    var date = today.getDate()+'-'+(today.getMonth()+1)+'-'+today.getFullYear();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    dummy[0].ScanDate = date+' '+time
    // console.log(dateTime);
    scanNewLog(listFingerprint)
    
    setTimeout(timeoutFunc, 10000);
  }
  
timeoutFunc();
